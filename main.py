from verifyExpression import validExpression
# Main Function
while True:
    # Taking Input from user
    bin_exp = input("Enter binary expression: ")
    # If user press Enter then break the loop.
    if bin_exp == '':
        break
    # If expression is valid then break the loop.
    if validExpression(bin_exp) == True:
        break

# keep running the code until user press Enter.
while bin_exp != '':
    
    # Spliting string into list by using space delimiter
    bin_exp = bin_exp.split()
    # Empty String to store result
    result = ''
    # Checking operator and producing result based on the operator provided by the user
    if bin_exp[1] == '&':
        # Traversing all bits of the operand
        for index in range(len(bin_exp[0])):
            # Implementing bitwise AND Function
            if bin_exp[0][index] == '1' and bin_exp[2][index] == '1':
                result += '1'
            else:
                result += '0'
                
    elif bin_exp[1] == '|':
        # Traversing all bits of the operand
        for index in range(len(bin_exp[0])):
            # Implementing bitwise OR Function        
            if bin_exp[0][index] == '1' or bin_exp[2][index] == '1':
                result += '1'
            else:
                result += '0'
                
    elif bin_exp[1] == '^':
        # Traversing all bits of the operand
        for index in range(len(bin_exp[0])):
            # Implementing bitwise XOR Function
            if bin_exp[0][index] != bin_exp[2][index]:
                result += '1'
            else:
                result += '0'
                
    # Display result
    print('  {}'.format(bin_exp[0]))
    print('{} {}'.format(bin_exp[1],bin_exp[2]))
    print('-'* (len(bin_exp[0]) + 2))
    print('  {}'.format(result))
            
    while True:
        # Taking Input from user
        bin_exp = input("Enter binary expression: ")
        # If user press Enter then break the loop.
        if bin_exp == '':
            break
        # If expression is valid then break the loop.
        if validExpression(bin_exp) == True:
            break
print('End of Program.')