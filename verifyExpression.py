def validExpression(expression):
    # Spliting string into list by using space delimiter
    exp_list = expression.split()
    # Defining valid operator
    valid_operators = ['&','|','^']
    # Count and sum no. of ones and zeros in operands.
    # If sum is not equal to length of the operand then operand contain digits other than binary.
    # If operand is not binary then display error and return False
    if len(exp_list[0]) != exp_list[0].count('0') + exp_list[0].count('1') or len(exp_list[2]) != exp_list[2].count('0') + exp_list[2].count('1'):
        print('Operand does not consist of binary digits.')
        return False
    # Check whether the length of operands 1 is equal to operands 2 or not.
    # If the length of operand is not equal then display error and return False.
    if len(exp_list[0]) != len(exp_list[2]):
        print('The operands are of different length.')
        return False
    # Check Whether the opearator provided by user is in list of valid operator or not.
    # If operator is invalid then display error and return False.
    if exp_list[1] not in valid_operators:
        print(exp_list[1], ' is invalid. Must be !, & or ^')
        return False
    # Check length of both operand.
    # If the operand have less than 2 digit then display error and return False.
    if len(exp_list[0]) < 2 or len(exp_list[2]) < 2:
        print('Length of the operands must be atleast 2 digits.')
        return False
    # If there is no error return True
    return True